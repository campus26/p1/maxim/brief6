# Qu'est-ce qu'un évènement en JS ?
C'est des retours sur des actions que le navigateur effectue

# Citez différents types d'évènements ?
- Un clic sur un bouton,
- Redimensionnement de fenêtre
- Appui d'une touche sur le clavier
- Une erreur qui survient

# Comment écouter un évènement de clic sur un bouton ?
```js
document.getElementById("monBouton").addEventListener("click", function() {
    document.getElementById("demo").innerHTML = "Hello World";
});
```

# Comment écouter le survol d'une div ?
```js
[...].addeventlistener("mouseover", [...]); // tant que la souris est dessus
[...].addeventlistener("mouseenter", [...]); // quand la souris entre dans l'élément
[...].addeventlistener("mouseleave", [...]); // quand la souris sort de l'élément
```

# Comment écouter lorsque l'utilisateur met son curseur dans un champ de formulaire ?
```js
[...].addeventlistener("focus", [...]); // quand le champ texte prend le focus
```

# Comment écouter lorsque l'on appuie sur une touche du clavier ?
```js
[...].addeventlistener("keydown", [...]); // quand la touche est appuyée
[...].addeventlistener("keyup", [...]); // quand la touche est relâchée
[...].addeventlistener("keypress", [...]); // quand du texte est entré
```

# Comment écouter la soumission d'un formulaire ?
```js
[...].addeventlistener("submit", [...]); // quand un formulaire est soumis
```

# Comment écouter lorsque l'utilisateur scroll sur la page ?
```js
window.addeventlistener("scroll", [...]); // quand on scroll sur la page
window.onscroll();
```

# Comment écouter la fin du chargement d'une page ?
```js
window.addeventlistener("ready", [...]); // quand la page est considérée "prête"
window.addeventlistener("load", [...]); // quand la page a fini de charger toutes les ressources
```

# Comment écouter lorsque l'utilisateur quitte la page ?
```js
window.addeventlistener("beforeunload", [...]); // quand la page est fermée
```
> Noter aussi que de nombreux navigateurs ignorent le résultat  de l'événement (il n'y a donc aucune demande de confirmation). Firefox a une préférence cachée dans about:config pour faire de même. 
[https://developer.mozilla.org/fr/docs/Web/Events/beforeunload]

# Comment écouter lorsque l'utilisateur change d'onglet ?
```js
window.addeventlistener("blur", [...]); // quand la page est passée en arrière plan
window.addeventlistener("focus", [...]); // quand la page est passée en premier plan
window.addeventlistener("visibilitychange", [...]); // quand la page change de visibilité
```

# Comment je supprime un évènement ?
```js
[...].removeEventListener(event, [...]);
```
