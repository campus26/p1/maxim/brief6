/*
    CREATES ALL THE OBJECTS AND ADDS THEM TO THE LEVEL (world)
*/

import { engine } from '../gameEngine.js';
/* 
    "If or switch" to implement later, to select the good level
    For now only first level is imported
*/
import { level } from './2/level.js';
import { player } from './2/player.js';
import { camera } from './2/camera.js';
import { saveScore } from './../scores/scores.js';

/*
    Creates the level
*/
export function levelsEngine() {

    // Admire your objects in the console!
    console.log(level);

    engine.world.gravity.y = level.gravity;

    // Init world's bodies
    level.entities.forEach(function (item) {

        //console.log(item);

        let body;
        switch (item.shape) {

            case 'tetrisName':
                let tetrisNA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisNB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisNC = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisND = Bodies.rectangle(item.x, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNE = Bodies.rectangle(item.x, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNF = Bodies.rectangle(item.x + 81, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNG = Bodies.rectangle(item.x, item.y + 243, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisNH = Bodies.rectangle(item.x, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNI = Bodies.rectangle(item.x + 81, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNJ = Bodies.rectangle(item.x + 162, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });


                body = Body.create({
                    parts: [tetrisNA, tetrisNB, tetrisNC, tetrisND, tetrisNE, tetrisNF, tetrisNG, tetrisNH, tetrisNI, tetrisNJ],
                    isStatic: item.isStatic,

                });
                break;

            case 'tetrisNameB':
                let tetrisNAB = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisNBB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisNCB = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisNDB = Bodies.rectangle(item.x, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNEB = Bodies.rectangle(item.x, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNFB = Bodies.rectangle(item.x + 81, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNGB = Bodies.rectangle(item.x, item.y + 243, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisNHB = Bodies.rectangle(item.x, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNIB = Bodies.rectangle(item.x + 81, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNJB = Bodies.rectangle(item.x + 162, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNKB = Bodies.rectangle(item.x + 162, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    tetrisNLB = Bodies.rectangle(item.x + 162, item.y + 243, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });


                body = Body.create({
                    parts: [tetrisNAB, tetrisNBB, tetrisNCB, tetrisNDB, tetrisNEB, tetrisNFB, tetrisNGB, tetrisNHB, tetrisNIB, tetrisNJB, tetrisNKB, tetrisNLB],
                    isStatic: item.isStatic,

                });
                break;


            case 'wallI':
                let wallIA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    wallIB = Bodies.rectangle(item.x, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    wallIC = Bodies.rectangle(item.x, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    wallID = Bodies.rectangle(item.x, item.y + 243, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    wallIE = Bodies.rectangle(item.x, item.y + 324, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    wallIF = Bodies.rectangle(item.x, item.y + 405, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });


                body = Body.create({
                    parts: [wallIA, wallIB, wallIC, wallID, wallIE, wallIF],
                    isStatic: item.isStatic,

                });
                break;


                case 'wallI-Sleep':
                let wallISA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    wallISB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    wallISC = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    wallISD = Bodies.rectangle(item.x + 243, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                    wallISE = Bodies.rectangle(item.x + 324, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),
                        wallISF = Bodies.rectangle(item.x + 405, item.y, item.width, item.height, {
    
                            render: { // options
                                fillStyle: item.fillStyle,
                                strokeStyle: item.strokeStyle
                            }
                        }),
    
                        wallISG = Bodies.rectangle(item.x + 486, item.y, item.width, item.height, {
    
                            render: { // options
                                fillStyle: item.fillStyle,
                                strokeStyle: item.strokeStyle
                            }
                        }),
    
                        wallISH = Bodies.rectangle(item.x + 567, item.y, item.width, item.height, {
    
                            render: { // options
                                fillStyle: item.fillStyle,
                                strokeStyle: item.strokeStyle
                            }
                        });

                body = Body.create({
                    parts: [wallISA, wallISB, wallISC, wallISD, wallISE, wallISF, wallISG, wallISH],
                    isStatic: item.isStatic,

                });
                break;

            

                
            case 'rectangle':

                body = Bodies.rectangle(item.x, item.y, item.width, item.height, {
                    isStatic: item.isStatic, // isStatic: true = gravity doesn't work on it
                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                });

                break;

          

            case 'circle':

                body = Bodies.circle(item.x, item.y, item.radius, {  //x,y,taille
                    //friction:0,
                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                });

                break;

        

            case 'tetrisI':
                let tetrisIA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisIB = Bodies.rectangle(item.x, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisIC = Bodies.rectangle(item.x, item.y + 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisID = Bodies.rectangle(item.x, item.y + 243, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisIA, tetrisIB, tetrisIC, tetrisID],
                    isStatic: item.isStatic,

                });
                break;

            case 'tetrisI-Sleep':
                let tetrisISA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisISB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisISC = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisISD = Bodies.rectangle(item.x + 243, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisISA, tetrisISB, tetrisISC, tetrisISD],
                    isStatic: item.isStatic,

                });
                break;



            case 'tetrisT-upside-down':
                let tetrisTupA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisTupB = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTupC = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTupD = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisTupA, tetrisTupB, tetrisTupC, tetrisTupD],
                    isStatic: item.isStatic,

                });
                break;


            case 'tetrisL-Left':
                let tetrisLLA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisLLB = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLLC = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLLD = Bodies.rectangle(item.x + 81, item.y - 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisLLA, tetrisLLB, tetrisLLC, tetrisLLD],
                    isStatic: item.isStatic,

                });
                break;

            case 'tetrisL-Right':
                let tetrisLRA = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisLRB = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLRC = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLRD = Bodies.rectangle(item.x + 81, item.y - 162, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisLRA, tetrisLRB, tetrisLRC, tetrisLRD],
                    isStatic: item.isStatic,

                });
                break;

                case 'tetrisZ-RightMove':
                let tetrisZRMA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisZRMB = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZRMC = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZRMD = Bodies.rectangle(item.x + 162, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisZRMA, tetrisZRMB, tetrisZRMC, tetrisZRMD],
                    isStatic: item.isStatic,

                });

                Events.on(engine, 'beforeUpdate', function(event) {
                   
            
                    var px = 400 + 100 * Math.sin(engine.timing.timestamp * item.speed);
            
                    Body.setPosition(body, {x: item.x - px, y: item.y });
                });
                

                break;


            case 'tetrisT':

                let tetrisTA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisTB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTC = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTD = Bodies.rectangle(item.x + 81, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisTA, tetrisTB, tetrisTC, tetrisTD],
                    isStatic: item.isStatic,

                });


                Events.on(engine, 'beforeUpdate', function (event) {

                    let py = 300 + 100 * Math.sin(engine.timing.timestamp * item.speed);

                    Body.setPosition(body, { x: item.x, y: item.y - py });

                });

                // World.add(engine.world, [compoundBodyA]);
                break;

            case 'tetrisTT':

                let tetrisTTA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisTTB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTTC = Bodies.rectangle(item.x + 162, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisTTD = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisTTA, tetrisTTB, tetrisTTC, tetrisTTD],
                    isStatic: item.isStatic,

                });


                Events.on(engine, 'beforeUpdate', function (event) {

                    let py = 300 + 100 * Math.sin(engine.timing.timestamp * item.speed);

                    Body.setPosition(body, { x: item.x, y: item.y - py });

                });

                // World.add(engine.world, [compoundBodyA]);
                break;


            case 'tetrisO':
                let tetrisOA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisOB = Bodies.rectangle(item.x, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisOC = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisOD = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisOA, tetrisOB, tetrisOC, tetrisOD],
                    isStatic: item.isStatic,

                });
                break;

            case 'tetrisLOrange':
                let tetrisLOA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisLOB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLOC = Bodies.rectangle(item.x, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLOD = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisLOA, tetrisLOB, tetrisLOC, tetrisLOD],
                    isStatic: item.isStatic,

                });
                break;

            case 'tetrisLBlue':
                let tetrisLBA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisLBB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLBC = Bodies.rectangle(item.x + 162, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisLBD = Bodies.rectangle(item.x + 162, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisLBA, tetrisLBB, tetrisLBC, tetrisLBD],
                    isStatic: item.isStatic,

                });

                break;

            case 'tetrisZ-Right':
                let tetrisZRA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisZRB = Bodies.rectangle(item.x + 81, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZRC = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZRD = Bodies.rectangle(item.x + 162, item.y - 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisZRA, tetrisZRB, tetrisZRC, tetrisZRD],
                    isStatic: item.isStatic,

                });


                break;


            case 'tetrisZ':
                let tetrisZA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisZB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZC = Bodies.rectangle(item.x + 162, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZD = Bodies.rectangle(item.x + 81, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisZA, tetrisZB, tetrisZC, tetrisZD],
                    isStatic: item.isStatic,

                });


                break;


            case 'tetrisZ-move':
                let tetrisZmA = Bodies.rectangle(item.x, item.y, item.width, item.height, {

                    render: { // options
                        fillStyle: item.fillStyle,
                        strokeStyle: item.strokeStyle
                    }
                }),
                    tetrisZmB = Bodies.rectangle(item.x + 81, item.y, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZmC = Bodies.rectangle(item.x + 162, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    }),

                    tetrisZmD = Bodies.rectangle(item.x + 81, item.y + 81, item.width, item.height, {

                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });

                body = Body.create({
                    parts: [tetrisZmA, tetrisZmB, tetrisZmC, tetrisZmD],
                    isStatic: item.isStatic,

                });

                Events.on(engine, 'beforeUpdate', function (event) {

                    Body.rotate(body, 0.01);




                    let py = 200 * Math.sin(engine.timing.timestamp * item.speed);

                    Body.setPosition(body, { x: item.x, y: item.y - py });

                });


                break;



            case 'bridge':

                body = Composites.stack(250, 50, 20, 1, 0, 0, function (x, y) {
                    return Bodies.circle(x, y, 10, Common.random(20, 40));
                });

                Composites.chain(body, 0, 0, 0, 0, {
                    stiffness: 1,
                    render: {
                        visible: false
                    }
                });

                let p1 = Constraint.create({
                    pointA: { x: 4170, y: 350 },
                    bodyB: body.bodies[0],

                    length: 2,
                    stiffness: 0.9
                });
                let p2 = Constraint.create({
                    pointA: { x: 4750, y: 350 },
                    bodyB: body.bodies[body.bodies.length - 1],

                    length: 2,
                    stiffness: 0.9
                })

                World.add(engine.world, [p1, p2]);
                console.log(body);

                break;

           

                case 'rectangleEnd':

                    body = Bodies.rectangle(item.x, item.y, item.width, item.height, {
                        isStatic: item.isStatic, // isStatic: true = gravity doesn't work on it
                        isSensor: item.isSensor,
                        id: "rectangleEnd",
                        render: { // options
                            fillStyle: item.fillStyle,
                            strokeStyle: item.strokeStyle
                        }
                    });
    
                    Events.on(engine, 'collisionStart', function (event) {
                        let pairs = event.pairs;
                        let colorA = 'orange';
                        let colorB = 'yellow';
                        pairs.forEach(function (pair) {
                            //console.log(pair);
                            if (pair.id === 'AplayerBrectangleEnd') {// THIS IS THE ONE TO BE ACTIVATED ON COLLISION
                                body.render.fillStyle = colorA;
                                // Send time to session storage
                                let playerTime = document.querySelector("#time").textContent;
                                sessionStorage.setItem('time', playerTime);
                                console.log(`sessionTime68 : ${playerTime}`);
                                // YOU WON THE LEVEL MAN!
                                document.location.href="end.html";
                            }
                            if (pair.id === 'ArectangleEndBplayer') {
                                body.render.fillStyle = colorB;
                                // Send time to session storage
                                let playerTime = document.querySelector("#time").textContent;
                                sessionStorage.setItem('time', playerTime);
                                console.log(`sessionTime75 : ${playerTime}`);
                                // YOU WON THE LEVEL MAN!
                                document.location.href="end.html";
                            }
    
    
                        })
                    });
    
    
                    break;
    




            // nothing more for now. We'll add here other shapes style when needed.
        }
        // Adds the objects to the corresponding world
        World.add(engine.world, body);
    })
    // Adds the player to the corresponding world
    World.add(engine.world, player);

    // Adds the camera to the corresponding world
    World.add(engine.world, camera);
}




/* run vertical, rotate, scale

var counter = 0,
                scaleFactor = 1.01;

            Events.on(engine, 'beforeUpdate', function(event) {
                counter += 1;



                if (scaleFactor > 1) {
                    //Body.rotate(body, 0.02);
                    //Body.scale(body, 0.995, 0.995);
                }

                var py = 300 + 100 * Math.sin(engine.timing.timestamp * 0.002);
                Body.setVelocity(body, { x: 0, y: py - body.position.y });
                Body.setPosition(body, { x: 100, y: py });


                });




                 Events.on(engine, 'beforeUpdate', function(event) {

                            Body.rotate(body, 0.01);




                        let py =  300 * Math.sin(engine.timing.timestamp * 0.002);
                        Body.setVelocity(body, { x: item.x, y: item.y });
                        Body.setPosition(body, { x: item.x, y: item.y -py });

                        });*/
