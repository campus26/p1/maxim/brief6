/* 
    TELLS THE CAMERA WHERE TO LOOK (here = at the player's position)
    Uncomment the last line to use
*/

import { engine, render } from '../../gameEngine.js';
import { player } from './player.js'; // used as the starting point of the camera's focus

export let camera = Events.on(engine, 'beforeUpdate', function (event) {

    // Camera on the player => DEACTIVATED FOR NOW
    Render.lookAt(render, player, { x: 300, y: 300 });

});
