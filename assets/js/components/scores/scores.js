/*
    Saves and retrieves scores from local storage    
*/

let playerName = sessionStorage.getItem('name');
let playerTime = sessionStorage.getItem('time');
console.log(`Player Time from session : ${playerTime}`);

export function saveScore() {

    let scores = JSON.parse(localStorage.getItem('Scores'));

    if (scores === null) {
        scores = [];
    }

    scores.push({
        name: playerName,
        score: playerTime
    });

    localStorage.setItem('Scores', JSON.stringify(scores));
}

export function getScore() {

    let scores = JSON.parse(localStorage.getItem('Scores'));

    let score = [];
    console.table(scores);
    if (scores === null) {
        return null
    } else {
        for (let i = 0; i < scores.length; i++) {
            score.push(scores[i]);
        }

        console.table(score);

        // Sort out scores

        score.sort(function compare(a, b) {
            if (a.score < b.score)
                return -1;
            if (a.score > b.score)
                return 1;
            return 0;
        });

        console.table(score);


        // Show scores 

        function showScore(element, index) {
            //console.log('array', array)
            let row = document.createElement("tr");
            row.innerHTML = `<td>#${(index + 1)}</td>`;
            row.innerHTML += `<td>${element.name}</td>`;
            row.innerHTML += `<td>${element.score}</td>`;
            document.querySelector("#scores").appendChild(row);
        }

        score.forEach(showScore);

        // fill-in h1 & form with the player's name and score in end.html
        document.querySelector("#player").innerHTML = playerName;
        document.querySelector("#score").innerHTML = playerTime;
        document.querySelector("#name").value = playerName;

    }
}

