/*
    THE END ♪♫ - Thanks for playing!
*/
import { saveScore } from './components/scores/scores.js';
import { getScore } from './components/scores/scores.js';

function stop() {
    if (sessionStorage.getItem('stateGame')) {
        saveScore();
        sessionStorage.removeItem('stateGame');
    }
    getScore();
}
stop();
