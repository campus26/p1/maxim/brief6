/* 
    CALLS AND RUN THE GAME
*/


/*
    Adds player name into the game
*/

document.querySelector("#name").innerText = sessionStorage.getItem("name");

/*
    Local storage Init


import { saveScores } from './components/scores/scores.js';
if (sessionStorage.getItem('time');)
saveScores();*/

/* 
    Game engine and render Init
*/

import { engine, render } from './components/gameEngine.js';
import { increaseTime } from './components/timer/timer.js';

var audio = new Audio('assets/audio/escapeball.wav');
audio.loop = true;

Engine.run(engine);
Render.run(render);

audio.play();

/* 
    Levels engine Init
*/

import { levelsEngine } from './components/levels/levelsEngine.js';

/*
    Game Init
*/

function start() {
    levelsEngine();
    setInterval(increaseTime, 10);
}
start(); // Let's play!
